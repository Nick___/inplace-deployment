import {
    BaseEntity,
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany
} from 'typeorm'
import MovieReview from '../movieReviews/MovieReview.model'

@Entity()
export default class MovieModel extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    title!: string

    @Column({ nullable: true })
    overview!: string

    @Column({ nullable: true })
    tagline!: string

    @Column({ type: 'time', nullable: true })
    runtime!: string

    @Column({
        type: 'timestamp',
        nullable: false,
        transformer: {
            from(value: string): Date {
                return new Date(value)
            },
            to(value: string): string {
                return value
            }
        }
    })
    release_date!: Date

    @Column({ nullable: true })
    revenue!: number

    @Column({ nullable: true })
    poster_path!: string

    @OneToMany(() => MovieReview, (review) => review.movie)
    reviews!: MovieReview[]
}
