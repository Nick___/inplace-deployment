export type MovieCreateDTO = {
    title: string
    overview?: string
    tagline?: string
    runtime: string
    release_date: Date
    revenue?: number
    poster_path: string
}
