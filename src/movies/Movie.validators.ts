import Joi from 'joi'

export const MovieCreateInputSchema = Joi.object({
    title: Joi.string().min(3).max(500).required(),
    overview: Joi.string().max(10000).allow(''),
    tagline: Joi.string().max(1000).allow('').optional(),
    release_date: Joi.date().required(),
    runtime: Joi.string()
        .regex(/^([0-9]{2})\:([0-9]{2}):([0-9]{2})$/)
        .required()
        .messages({
            'string.regex': 'Runtime does not matches the format HH:MM:SS'
        }),
    revenue: Joi.number(),
    poster_path: Joi.string().max(800).allow('')
})
// HINT: this is how you can use OOP principle for validators

export const MovieUpdateInputSchema = MovieCreateInputSchema.concat(
    Joi.object({
        title: Joi.string().max(500).optional(),
        overview: Joi.string().max(10000).optional().allow(''),
        tagline: Joi.string().max(1000).optional().allow(''),
        release_date: Joi.date().optional(),
        runtime: Joi.string()
            .regex(/^([0-9]{2})\:([0-9]{2}):([0-9]{2})$/)
            .required()
            .messages({
                'string.regex': 'Runtime does not matches the format HH:MM:SS'
            })
            .optional(),
        revenue: Joi.number().optional(),
        poster_path: Joi.string().max(800).optional().allow('')
    })
)
