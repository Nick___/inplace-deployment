import { Server } from 'http'
import { DataSource } from 'typeorm'

export interface DbConfig {
    port: number
    host: string
    username: string
    password: string
}

export interface ServerConfig {
    port?: number
    db: DbConfig
}

export interface ControllerError {
    error_id: string
    message: string
}

export function isControllerError(value: any): value is ControllerError {
    return value.error_id !== undefined
}

export interface ApplicationServer {
    app: Server
    dbClient: DataSource | undefined
}
