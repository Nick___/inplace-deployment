import { faker } from '@faker-js/faker'
import MovieModel from '../../movies/Movie.model'

export default async function createFakeMovie() {
    const movieData = MovieModel.create({
        release_date: faker.date.past(),
        title: faker.lorem.sentence(5),
        overview: faker.lorem.paragraph(10),
        tagline: faker.hacker.phrase(),
        runtime: '01:34:00',
        revenue: Number(faker.random.numeric(6)),
        poster_path: faker.image.imageUrl()
    })

    const movie = await movieData.save()

    return movie
}
