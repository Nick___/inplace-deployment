import { GenericContainer, StartedTestContainer } from 'testcontainers'
import path from 'path'

export default async function createTestDbContainer(): Promise<StartedTestContainer> {
    return await new GenericContainer('postgres')
        .withEnv('POSTGRES_USER', 'root') // db user
        .withEnv('POSTGRES_PASSWORD', 'theSeniorDev') // db password
        .withEnv('POSTGRES_DB', 'movie-api-db') // database name
        .withExposedPorts(5432)
        .start()
}
