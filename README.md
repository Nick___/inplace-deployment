# Action Item: Deployment to `Virtual Servers` 🛰

In this `Action Item` you will:

-   🔋 Deploy an `API Server` using virtual servers from `AWS EC2`
-   💾 Use `AWS RDS` to set up a `cloud database`
-   🤖 Use `GitLab` to set up `an automatic release pipeline`

## Challenges:

1. [x] 💪[COMPETENT] Get an account on `GitLab`
2. [ ] 💪[COMPETENT] Get an `AWS` account
3. [ ] 💪[COMPETENT] Create a `GitLab` repository for the `API`
4. [ ] 💪[COMPETENT] Create a `cloud database` in `AWS RDS` for our production environment
5. [ ] 💪[COMPETENT] Create a `virtual server(instance)` in `EC2` and deploy to it manually
6. [ ] 🏋🏽‍♀️[PROFICIENT] Automatize the deployment by creating a pipeline in `Gitlab`
7. [ ] 🎁[BONUS] Add a `staging` instance and `database` and a new stage to the pipeline

> Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.

---

### <a name="app-structure"></a> App Architecture

![app-structure](examples/app_structure/app_structure.png)

---

#### ⚠️ Avoid Accidental Charges from AWS ⚠️

In this `Action Item` we will use AWS service in their [Free Tier](https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=categories%23databases) that are offered free of charge inside certain limits. To make sure you wont be charged by AWS:

1) Check the Billing Dashboard reglarry - [How to use the Billling Dashboard](https://www.loom.com/share/0d55730aa26d4a4f947cc4a89c1b4408)
2) Setup a Budget and Email alerts for yourself - [How to setup a Budget in AWS](https://www.loom.com/share/2cb3a3e78d10477991fa0e3429c9ff00)

#### Clean Up After the Action Item:

1) Delete the *EC2 Instance*: [Video Guide - Cleanup EC2](https://www.loom.com/share/88d790d731204e478d4647276e742dd9)
2) Delete the *RDS Database*: [Video Guide- Cleanup RDS](https://www.loom.com/share/de9b0914bda94664b1f614a190fd8123) 

#### ⚠️ Make sure you check the video above to avoid any unwanted charges at the end of the month! ⚠️
---

<details closed>
<summary>CLICK ME! - SOLUTION: 1. Get an account on GitLab</summary>

## 1. Get an account on `GitLab`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 2. Get an AWS account</summary>

## 2. Get an account on `AWS`

If you do not have one already, check the [SUPPORT.md](SUPPORT.md) file in this repository.

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 3. Create a GitLab repository for the API</summary>

## 3. Create a `GitLab` repository for the backend app

Check the [SUPPORT.md](SUPPORT.md) file in this repository for `step-by-step` instructions.

-   [Video Walkthrough - Move the code repository to GitLab](https://www.loom.com/share/80e2acada3494ed0ba3270cc280ee7eb)

⚠️Before proceeding make sure your `API` source code is on `GitLab`.⚠️

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 4. Create a cloud database in AWS RDS for our production environment</summary>

### 4. Create a `Cloud Database` in `AWS RDS` for our `production` environment

In production, we will use a `Cloud Database` to host our `application`. To create the `database`:

#### 4.1 Create the Database in `RDS`

[Video Walkthrough - Create a Database in `RDS`](https://www.loom.com/share/32e1a90088e64563bcdd067316f3bd7e)

#### 4.2 Connect to the `database` from your `local environment`

[Video Walkthrough - Connect to the `RDS` database using PG Admin](https://www.loom.com/share/5d786541729d4b99a9b1aab830229e6e)

#### 4.3 Add the database connection data to the `.env` file

```diff
# DB Config
+DB_PORT=5432
+DB_HOST=database-api-movie-production.cep4ujn358dp.eu-central-1.rds.amazonaws.com
+DB_USERNAME=root
+DB_PASSWORD=theSeniorDev
```

#### 4.4 Create a `Movie` and check it lives in the `cloud database`

Go to [localhost:3000/api-docs](http://localhost:3000/api-docs) and used the `Swagger UI` to create a movie.

-   [Video Walkthrough - Connect the `API` to the database](https://www.loom.com/share/ebd7cbf5bdb0474aa958631a94b4e5b6)

##### 4.4.1 Get a `token` from `Auth0` for your application

To make requests to the `API` you need an `access token`.

-   [Video Walkthrough - Obtain a `test` Auth token](https://www.loom.com/share/842f066f733c4d1db3e9b12e744942c8)

##### 4.4.1 Make a `POST` request using `Swagger`

-   [Video Walkthrough - Test the connection by adding a movie](https://www.loom.com/share/8b328727d54a44049c6e246c2c26ae31)

##### 4.4.1 Check the production tables in `PgAdmin`

-   [Video Walkthrough - Check the production DB Schema with PG Admin](https://www.loom.com/share/5d786541729d4b99a9b1aab830229e6e)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 5. Create a Virtual Server(instance) in EC2 and, deploy to it manually</summary>

### 5. Create a virtual server in `EC2`, install `node.js`, and, deploy to it manually

#### 5.1 Create a `linux` virtual server(instance) on `EC2`:

-   [Video Walkthrough - Create an `EC2 Instance`](https://www.loom.com/share/2c6fd3a05b2341fa870599ab33a3480c)

#### 5.2 Connect to the virtual server on `EC2` with `ssh`, install `node.js` and upload the files

You can follow the [official documentation here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html) or watch the video below.

-   [Video Walkthrough - Install Node.js on an `EC2` instance](https://www.loom.com/share/aa791bfca2bc47daac371f27ab61793d)

#### 5.3 Upload the code with `scp` and deploy it manually

You can now use `scp` to upload your code to the `virtual instance`. Replace `AWS_KEY_PATH` with the local path to the `.pem` file on your computer and `INSTANCE_IP` with the public IP of your instance and run the commands below:

```bash
# set right permissions on your key
chmod 400 AWS_KEY_PATH
# copy files to the ec2 instance
scp -i movie-api-instance-prod-key.pem -r build ec2-user@52.59.194.108:/home/ec2-user/build
scp -i movie-api-instance-prod-key.pem -r package.json ec2-user@52.59.194.108:/home/ec2-user/package.json

scp -i AWS_KEY_PATH package.json ec2-user@INSTANCE_IP:/home/ec2-user/build/package.json
```

#### 5.4 Create a `.env` file in the `AWS` instance

Run the `nano` editor in the `terminal` of the instance, inside the build folder:

```bash
nano .env
```

And ad inside the content of your local `.env` file.

#### 5.5 Install `pm2` on the `instance` and run your app:

_Note: `PM2` runs `node.js` as a background process, so the app is live, even when we close our `ssh` session. We will get deeper into how it works later on._

```bash
npm install pm2 -g
# in the ./build folder on the virtual machine -- instance
pm2 start index.js -i max
```

#### 5.6 Check the application is now live on the `EC2` instance:

Go to [http://YOUR_INSTANCE_PUBLIC_IP/api-docs/](http://INSTANCE_IP/api-docs/)

-   [Video Walkthrough - Upload the code to an `EC2 Instance and` run the API manually](https://www.loom.com/share/e5c4044e364c4b0ba97ea5464bae0063)

⚠️If you get a connection error to the database while running the code on the `ec2` machine it might be because of the security group. To fix it, follow the video below.⚠️

-   [Video Walkthrough - Connect the `RDS` database with the `EC2` instance](https://www.loom.com/share/ec7bf6e4022143ddb07478bef2405de2)

</details>

---

<details closed>
<summary>CLICK ME! - SOLUTION: 6. Automatize the deployment by creating a pipeline in Gitlab</summary>

### 6. Automatize the `deployment` by creating a `pipeline` in `Gitlab`

We have been able to `deploy` manually but is very time-consuming and error-prone.

Let's automate the deployment using `Gitlab`.

#### 6.1. In `Gitlab` go to pipelines:

![Create Pipeline Step 1](examples/add_pipeline_step_one.png)

#### 6.2. Click **Create New Pipeline** && remove the default code:

![Create Pipeline Step 2](examples/add_pipeline_step_two.png)

#### 6.3. Add the following code instead:

⚠️Inspect all the steps of the pipeline and make sure you understand the syntax.⚠️

```yaml
image: node:16
stages: ['build', 'test', 'deploy']

# WARNING
# This pipeline needs the following variables set up to work:
# INSTANCE_IP =  // the public IP of the AWS instance
# SECRET_KEY = // the secret key to connect to the AWS instance (.pem) file
# ENV_FILE = // the .env file for production

cache:
    policy: pull-push
    untracked: false
    when: on_success
    paths:
        - 'node_modules'

# the build job
build_job:
    stage: build
    script:
        - npm install
        - npm run build
    artifacts:
        paths:
            - build
        expire_in: 1 week

test_job:
    stage: test
    image: node:16

    services: # we need docker in order to be able to run the tests
        - name: docker:20.10.1-dind
          command: ['--tls=false', '--host=tcp://0.0.0.0:2376']

    variables:
        DOCKER_HOST: 'tcp://docker:2375'
        DOCKER_TLS_CERTDIR: ''
        DOCKER_DRIVER: 'overlay2'

    script:
        - export CI=true
        - npm ci
        - npm test
    needs:
        - build_job
    dependencies:
        - build_job

deploy_job:
    stage: deploy
    only:
        - main
    before_script: # prepare the pipeline runner for deploy by installing ssh
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
        - apt-get update -y
        - apt-get -y install rsync
    script:
        - chmod 400 $SECRET_KEY
        # clean up the ec32 instance
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'

        # copy files to the ec2 instance
        - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api
        - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
        - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh

        # copy .env to the ec2 instance
        - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/.env.production

        # run the deploy script
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
    needs:
        - job: build_job
          artifacts: true
        - job: test_job
          artifacts: false
    dependencies:
        - build_job
        - test_job
```

-   [Video Walkthrough - Add a pipeline to the API](https://www.loom.com/share/3b25f748f1954f1ab1b4ccd05abb93ca)

---

#### 6.4 Add a `deploy.sh` script to be executed by Gitlab in the deployment job:

Create the `deploy.sh` file in the root folder of the repository:

```bash
touch deploy.sh
```

An add to it the following code:

```bash
# clean up old app
pm2 kill

# Setting up node ENV to production
export NODE_ENV=production

# installing dependencies
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run prod
```

#### 6.5 Add the production and monitor script to `package.json`:

```diff
  "scripts": {
    "build": "rm -rf build && tsc && npm run copy-files",
    "test": "jest",
+   "prod": "NODE_ENV=production & pm2 start build/index.js  --name 'api' -f",
    "start": "NODE_ENV=development nodemon src/index.ts",
    "copy-files": "copyfiles -u 1 src/**/*.yaml build/ & copyfiles -u 1 src/**/*.graphql build/",
    "orm:migrate-gen": "typeorm-ts-node-commonjs migration:generate ./src/database/migrations/movie-api -d ./src/database/ApiDataSource.ts ",
    "orm:migrate-run": "typeorm-ts-node-commonjs -d ./src/database/ApiDataSource.ts migration:run"
  },
```

#### 6.6 Add the missing variables the `ci-cd` file needs: INSTANCE_IP, SECRET_KEY, and ENV_FILE

##### 6.6.1 Add `INSTANCE_IP` to the `Gitlab` variables

![Add INSTANCE_IP](examples/add_instance_ip_variable.png)

##### 6.6.2 Add `SECRET_KEY` to the `Gitlab` variables

![Add SECRET_KE](examples/add_secret_key_variable.png)

##### 6.6.3 Add `ENV_FILE` to the `Gitlab` variables

![Add ENV_FILE](examples/add_env_file_variable.png)

##### 6.6.4 Push code or run the `pipeline` manually

```bash
git push
```

-   [Video Walkthrough - Add `pipeline variables` and the `deploy.sh file`](https://www.loom.com/share/054f69353017470680a80f7aa7c8b61a)

#### 6.7 If the pipeline does not run automatically, run the pipeline manually:

![Run the pipeline](examples/add_pipeline_step_sixteen.png)

-   [Video Walkthrough - Run the pipeline manually](https://www.loom.com/share/8aeb5c143f9b412a966328f9a043d950)

#### 6.8 When the `pipeline` is finished you should be live:

-   [Video Walkthrough - Pipeline running successfully](https://www.loom.com/share/21450c99475e4283bf2c0e7016cf0467)

**Congratulations!!! You can now run applications in the `cloud` and deploy them automatically!**

</details>

---

<details closed>
<summary>CLICK ME! - 7. SOLUTION: Add a staging instance and database and update the pipeline</summary>

### 7. Add a `staging instance` and a `staging database` and update the pipeline

It would also be great if we could add a `rollback` mechanism or a `staging` environment.

##### 7.1. Add a new `staging instance` in AWS following the same process we did for the production instance

-   [Video Walkthrough - Provision a STAGING Instance](https://www.loom.com/share/035791e3520d488895bc557afdaf1a71)

> NOTE: you can use the prefix `-staging` to make sure you don't confuse both instances

##### 7.2. Add a new `staging database` in RED following the same process we did for the production database

-   [Video Walkthrough - Provision a STAGING Database](https://www.loom.com/share/1d18d9fc019a46878eedfd9cf5819f37)

> NOTE: you can use the prefix `-staging` to make sure you don't confuse the databases

##### 7.3. Add different `environments` in `GitLab`

Add different `environments` to be able to run the `pipeline` jobs with different values for the pipeline variables depending on the `environment`.

![multi-environments](examples/task_7/multienvironmments.png)

##### 7.4. Update the `pipeline` variables to match the environments

![env-variables](examples/task_7/env-variables-multistage.png)

-   [Video Walkthrough - Set up the `multistage pipeline` using `variables` and `environments`](https://www.loom.com/share/c5924b4e081e43c68de7c28e8652d3e4)

See the final `pipeline declaration` below:

```yaml
image: node:16
stages: ['build', 'test', 'deploy']

# WARNING
# This pipeline needs the following variables set up to work:
# INSTANCE_IP =  // the public IP of the AWS instance
# SECRET_KEY = // the secret key to connect to the AWS instance (.pem) file
# ENV_FILE = // the .env file for production

cache:
    policy: pull-push
    untracked: false
    when: on_success
    paths:
        - 'node_modules'

# the build job
build_job:
    stage: build
    script:
        - npm install
        - npm run build
    artifacts:
        paths:
            - build
        expire_in: 1 week

test_job:
    stage: test
    image: node:16

    services: # we need docker in order to be able to run the tests
        - name: docker:20.10.1-dind
          command: ['--tls=false', '--host=tcp://0.0.0.0:2376']

    variables:
        DOCKER_HOST: 'tcp://docker:2375'
        DOCKER_TLS_CERTDIR: ''
        DOCKER_DRIVER: 'overlay2'

    script:
        - export CI=true
        - npm ci
        - npm test
    needs:
        - build_job
    dependencies:
        - build_job

deploy_job_staging:
    stage: deploy
    environment:
        name: staging
        url: http://ec2-52-59-194-108.eu-central-1.compute.amazonaws.com
    only:
        - staging
    before_script: # prepare the pipeline runner for deploy by installing ssh
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
        - apt-get update -y
        - apt-get -y install rsync
    script:
        - chmod 400 $SECRET_KEY
        # clean up the ec32 instance
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'

        # copy files to the ec2 instance
        - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api
        - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
        - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh

        # copy .env to the ec2 instance
        - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/.env.production

        # run the deploy script
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
    needs:
        - job: build_job
          artifacts: true
        - job: test_job
          artifacts: false
    dependencies:
        - build_job
        - test_job

deploy_job_production:
    stage: deploy
    environment:
        name: production
        url: http://ec2-52-59-194-108.eu-central-1.compute.amazonaws.com
    only:
        - main
    before_script: # prepare the pipeline runner for deploy by installing ssh
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - mkdir -p ~/.ssh
        - chmod 700 ~/.ssh
        - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
        - apt-get update -y
        - apt-get -y install rsync
    script:
        - chmod 400 $SECRET_KEY
        # clean up the ec32 instance
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'rm -rf /home/ec2-user/api'
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'mkdir /home/ec2-user/api'

        # copy files to the ec2 instance
        - scp -i $SECRET_KEY -r build ec2-user@$INSTANCE_IP:/home/ec2-user/api
        - scp -i $SECRET_KEY package.json ec2-user@$INSTANCE_IP:/home/ec2-user/api/package.json
        - scp -i $SECRET_KEY deploy.sh ec2-user@$INSTANCE_IP:/home/ec2-user/api/deploy.sh

        # copy .env to the ec2 instance
        - scp -i $SECRET_KEY $ENV_FILE ec2-user@$INSTANCE_IP:/home/ec2-user/api/.env.production

        # run the deploy script
        - ssh -i $SECRET_KEY ec2-user@$INSTANCE_IP 'cd /home/ec2-user/api && bash deploy.sh'
    needs:
        - job: build_job
          artifacts: true
        - job: test_job
          artifacts: false
    dependencies:
        - build_job
        - test_job
```

##### 7.5. Demo of a `multistage pipeline`

The final pipeline will look like this:

![multistage-pipeline](examples/task_7/multistage-pipeline.png)

-   [Video Walkthrough - Using `Merge Requests` to move code from `staging` to `production`](https://www.loom.com/share/2aab48c2d6054e46a050f8d7f9822992)

</details>

---

#### ⚠️ Clean Up After the Action Item:

1) Delete the *EC2 Instance*: [Video Guide - Cleanup EC2](https://www.loom.com/share/88d790d731204e478d4647276e742dd9)
2) Delete the *RDS Database*: [Video Guide- Cleanup RDS](https://www.loom.com/share/de9b0914bda94664b1f614a190fd8123) 

#### ⚠️ Make sure you check the videos above to avoid any unwanted charges at the end of the month! ⚠️

### Getting Help

If you have issues with the Action Item, you can ask for help in the [Community](https://community.theseniordev.com/) or in the [Weekly Q&As](https://calendar.google.com/calendar/u/0?cid=Y19kbGVoajU1Z2prNXZmYmdoYmxtdDRvN3JyNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

### Made with :orange_heart: in Berlin by @TheSeniorDev
